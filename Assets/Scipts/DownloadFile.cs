﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using Vuforia;

public class DownloadFile : MonoBehaviour
{ 
    public bool success = false;

    public IEnumerator Download(string remotePath, string localPath)
    {
        UnityWebRequest unityWebRequest = new UnityWebRequest(remotePath);
        unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
        yield return unityWebRequest.SendWebRequest();

        if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
        {
            Debug.Log("Unable to download " + remotePath + ": unityWebRequest.error: " + unityWebRequest.error);
            yield break;
        }
        else
        {
            Debug.Log("Successfully finished downloading " + remotePath + " to " + localPath);
            File.WriteAllBytes(localPath, unityWebRequest.downloadHandler.data);
            success = true;
        }

        yield return null;
    }
}
