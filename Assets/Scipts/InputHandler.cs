﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using PostWithComments;
using UnityEngine;
using UnityEngine.UI;

public class InputHandler : MonoBehaviour
{
    private Vector3 inputRotateStartPosition;

    private Vector3 inputScaleStartPosition1 = Vector3.zero;
    private Vector3 inputScaleStartPosition2 = Vector3.zero;

    private Vector3 pictureViewerImageStartPosition;
    private Vector3 pictureViewerImageStartScale;

    private CommentHandler commentInViewer;
    private CustomTrackableEventHandler customTrackableEventHandlerInViewer;

    // Start is called before the first frame update
    void Start()
    {
        // This is important avoid triggering mouse events on touch devices
        Input.simulateMouseWithTouches = false;
    }

    // Update is called once per frame
    void Update()
    {
        // Capture a screenshot with P
        if (Input.GetKeyDown(KeyCode.P))
        {
            ScreenCapture.CaptureScreenshot("Screenshots/SS_" + System.DateTime.Now.ToString("dd-MM-yy_hh-mm-ss") + ".png");
        }

        bool mouseDownButNotOverGameObject = Input.GetMouseButtonDown(0) &&
            !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject();

        bool touchDownButNotOverGameObject = Input.touchCount == 1 &&
            Input.touches[0].phase == TouchPhase.Began &&
            !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId);

        // Check if a touch is beeing performed
        // and
        // Do not perform ray casts through buttons and other GUI objects
        if (mouseDownButNotOverGameObject || touchDownButNotOverGameObject)
        {
            Vector3 inputPosition;
            if (Input.GetMouseButtonDown(0))
            {
                inputPosition = Input.mousePosition;
            }
            else
            {
                inputPosition = Input.touches[0].position;
            }

            Ray ray = Camera.main.ScreenPointToRay(inputPosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                Debug.Log("We hit '" + hit.transform.gameObject.name + "'");

                CommentHandler commentHandlerHit = hit.transform.gameObject.GetComponent<CommentHandler>();

                if (commentHandlerHit)
                {
                    commentInViewer = commentHandlerHit;
                    Main.instance.currentCustomTrackableEventHandlerInPictureViewer = Main.instance.currentCustomTrackableEventHandler;
                    ResetImageScaleAndPositionAndRotation();
                    StartCoroutine(commentHandlerHit.GetAndShowLargeImage());
                    Main.instance.pictureViewer.SetActive(true);
                    customTrackableEventHandlerInViewer = Main.instance.currentCustomTrackableEventHandler;

                }
            }
        }

        // Move starts
        bool mouseMoveFirstPositionSet = Input.GetMouseButtonDown(0);
        bool touchMoveOneFingerFirstPositionSet = Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Began;

        if (mouseMoveFirstPositionSet || touchMoveOneFingerFirstPositionSet)
        {
            if (mouseMoveFirstPositionSet)
            {
                inputRotateStartPosition = Input.mousePosition;
            }
            else
            {
                inputRotateStartPosition = Input.touches[0].position;
            }

            pictureViewerImageStartPosition = Main.instance.pictureViewerImage.transform.position;
        }

        // Move in progress
        bool mouseMoveInProgress = Input.GetMouseButton(0);
        bool touchMoveOneFingerInProgress = Input.touchCount == 1 && Input.touches[0].phase == TouchPhase.Moved;

        if (mouseMoveInProgress || (Input.touchCount == 1 && touchMoveOneFingerInProgress))
        {
            Vector3 inputRotateCurrentPosition;

            if (mouseMoveInProgress)
            {
                inputRotateCurrentPosition = Input.mousePosition;
            }
            else
            {
                inputRotateCurrentPosition = Input.touches[0].position;
            }

            // We want the horizontal travel of the pointer for the prefab rotation on the x axis
            float horizontalDistance = inputRotateStartPosition.x - inputRotateCurrentPosition.x;
            float verticalDistance = inputRotateStartPosition.y - inputRotateCurrentPosition.y;

            float x = pictureViewerImageStartPosition.x;
            float y = pictureViewerImageStartPosition.y;

            Main.instance.pictureViewerImage.transform.position = new Vector3(
                x -= horizontalDistance,
                y -= verticalDistance,
                0);
        }

        // When going back from two touches to one touch, we need to reset the inputRotateStartPosition to avoid a rotation jump
        if (Input.touchCount == 2 && (Input.touches[0].phase == TouchPhase.Ended || Input.touches[1].phase == TouchPhase.Ended))
        {
            if (Input.touches[0].phase == TouchPhase.Ended)
            {
                inputRotateStartPosition = Input.touches[1].position;
            }
            else
            {
                inputRotateStartPosition = Input.touches[0].position;
            }
        }

        // Scale start, first finger down
        // For touchScaleFirstPositionSet it is important not to check Input.touchCount == 1 because touching the screen with two fingers during the same frame happens and results in glitches if first finger position is an old position 
        bool mouseScaleFirstPositionSet = Input.GetMouseButtonDown(2) && Input.GetKey(KeyCode.LeftControl);
        bool touchScaleFirstPositionSet = Input.touchCount != 0 && Input.touches[0].phase == TouchPhase.Began;

        if (mouseScaleFirstPositionSet || touchScaleFirstPositionSet)
        {
            if (Input.GetMouseButtonDown(2) && Input.GetKey(KeyCode.LeftControl))
            {
                // Placed first mouse position
                inputScaleStartPosition1 = Input.mousePosition;
            }
            else
            {
                // Placed first finger
                inputScaleStartPosition1 = Input.touches[0].position;
            }
        }

        // Scale start, second finger down
        bool mouseScaleSecondPositionSet = Input.GetMouseButtonDown(2) && !Input.GetKey(KeyCode.LeftControl);
        bool touchScaleSecondPositionSet = Input.touchCount == 2 && Input.touches[1].phase == TouchPhase.Began;

        if (mouseScaleSecondPositionSet || touchScaleSecondPositionSet)
        {
            if (mouseScaleSecondPositionSet)
            {
                // Placed second mouse position
                inputScaleStartPosition2 = Input.mousePosition;

                if (inputScaleStartPosition1 == Vector3.zero)
                {
                    Debug.Log("First finger position unset. Use Ctrl + Middle mouse to do so.");
                }
            }
            else
            {
                // Placed second finger
                inputScaleStartPosition2 = Input.touches[1].position;
            }

            pictureViewerImageStartScale = Main.instance.pictureViewerImage.transform.localScale;
        }

        // Scale in progress, two fingers down, none started
        bool mouseScaleInProgress = Input.GetMouseButton(2) && !Input.GetKey(KeyCode.LeftControl) && inputScaleStartPosition1 != Vector3.zero;
        bool touchScaleInProgress = Input.touchCount == 2 &&
            Input.touches[0].phase != TouchPhase.Began &&
            Input.touches[1].phase != TouchPhase.Began &&
            (Input.touches[0].phase == TouchPhase.Moved || Input.touches[1].phase == TouchPhase.Moved);

        if (mouseScaleInProgress || touchScaleInProgress)
        {
            Vector3 inputScaleCurrentPosition1;
            Vector3 inputScaleCurrentPosition2;

            if (mouseScaleInProgress)
            {
                // Since we don't have two mice, we need to take the first position as first finger
                inputScaleCurrentPosition1 = inputScaleStartPosition1;
                inputScaleCurrentPosition2 = Input.mousePosition;
            }
            else
            {
                inputScaleCurrentPosition1 = Input.touches[0].position;
                inputScaleCurrentPosition2 = Input.touches[1].position;
            }

            float initialDistance = Vector3.Distance(inputScaleStartPosition1, inputScaleStartPosition2);
            float currentDistance = Vector3.Distance(inputScaleCurrentPosition1, inputScaleCurrentPosition2);

            // If fingers moved appart, deltadistance is positive, otherwise negative  
            float deltaDistenace = currentDistance - initialDistance;

            // The maximum distance while using two fingers from
            // * the edges of the screen to the center is -(Screen.width / 2)
            // * the center of the screen to the edges is Screen.width / 2
            // By mapping this range to [.5f, 1.5f], one maximized horizontal pinch doubles or halves the prefab
            float scale = MapRange(deltaDistenace, -(Screen.width / 2), Screen.width / 2, .5f, 1.5f);

            // Debug.Log("Scaling by: " + scale);
            Main.instance.pictureViewerImage.transform.localScale = new Vector3(
                pictureViewerImageStartScale.x * scale,
                pictureViewerImageStartScale.y * scale,
                1);
        }
    }

    public static float MapRange(float value, float inputRangeFrom, float inputRangeTo, float outputRangeFrom, float outputRangeTo)
    {
        return (value - inputRangeFrom) * (outputRangeTo - outputRangeFrom) / (inputRangeTo - inputRangeFrom) + outputRangeFrom;
    }

    public void ResetImageScaleAndPositionAndRotation()
    {
        Main.instance.pictureViewerImage.transform.localPosition = new Vector3(0, 0, 0);
        Main.instance.pictureViewerImage.transform.localScale = new Vector3(1, 1, 1);
        Main.instance.pictureViewerImage.transform.rotation = Quaternion.Euler(0, 0, 0);
    }

    public void RotateImageClockwise()
    {
        Vector3 oldRotation = Main.instance.pictureViewerImage.transform.rotation.eulerAngles;

        Main.instance.pictureViewerImage.transform.rotation = Quaternion.Euler(oldRotation.x, oldRotation.y, oldRotation.z - 90);
    }

    public void RotateImageCounterClockwise()
    {
        Vector3 oldRotation = Main.instance.pictureViewerImage.transform.rotation.eulerAngles;

        Main.instance.pictureViewerImage.transform.rotation = Quaternion.Euler(oldRotation.x, oldRotation.y, oldRotation.z + 90);
    }

    public void LoadNextImage()
    {
        int commentToLoadIndex;
        int commentInViewerIndex = customTrackableEventHandlerInViewer.commentHandlers.IndexOf(commentInViewer);

        if (commentInViewerIndex == customTrackableEventHandlerInViewer.commentHandlers.Count - 1)
        {
            commentToLoadIndex = 0;
        }
        else
        {
            commentToLoadIndex = commentInViewerIndex + 1;
        }

        ResetImageScaleAndPositionAndRotation();
        commentInViewer = customTrackableEventHandlerInViewer.commentHandlers[commentToLoadIndex];
        StartCoroutine(customTrackableEventHandlerInViewer.commentHandlers[commentToLoadIndex].GetAndShowLargeImage());
        Main.instance.pictureViewer.SetActive(true);
    }

    public void LoadPreviousImage()
    {
        int commentToLoadIndex;
        int commentInViewerIndex = customTrackableEventHandlerInViewer.commentHandlers.IndexOf(commentInViewer);

        if (commentInViewerIndex == 0)
        {
            commentToLoadIndex = customTrackableEventHandlerInViewer.commentHandlers.Count -1;
        }
        else
        {
            commentToLoadIndex = commentInViewerIndex - 1;
        }

        ResetImageScaleAndPositionAndRotation();
        commentInViewer = customTrackableEventHandlerInViewer.commentHandlers[commentToLoadIndex];
        StartCoroutine(customTrackableEventHandlerInViewer.commentHandlers[commentToLoadIndex].GetAndShowLargeImage());
        Main.instance.pictureViewer.SetActive(true);
    }
}