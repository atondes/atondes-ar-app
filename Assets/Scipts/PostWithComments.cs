﻿using System.Collections.Generic;

namespace PostWithComments
{
    [System.Serializable]
    public class Comment
    {
        public string text;
        public string original_image;
        public string preview_image;
        public string large_image;
    }

    [System.Serializable]
    public class Post
    {
        public string meta_value;
        public string post_title;
        public List<Comment> commentList;
        public string event_link;
    }

    [System.Serializable]
    public class RootObject
    {
        public List<Post> postList;
    }
}