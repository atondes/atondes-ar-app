﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using PostWithComments;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;
using Image = UnityEngine.UI.Image;

public class Main : MonoBehaviour
{
    public static Main instance = null;
    public float currentTrackableWidth;
    public float currentTrackableHeight;
    public CustomTrackableEventHandler currentCustomTrackableEventHandler;
    public CustomTrackableEventHandler currentCustomTrackableEventHandlerInPictureViewer;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;

            DontDestroyOnLoad(this);
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    private bool areCustomTrackableEventHandlersInitialized = false;

    private static string remoteMarkerDat = "https://rotondes.next-site.de/files/ATONDES.dat";
    private static string remoteMarkerXml = "https://rotondes.next-site.de/files/ATONDES.xml";

    private string localMarkerDat;
    private string localMarkerXml;

    private CustomTrackableEventHandler customTrackableEventHandler;

    private DownloadFile downloadFileMarkerDat;
    private DownloadFile downloadFileMarkerXml;

    public GameObject pictureViewer;
    public Image pictureViewerImage;
    public Text pictureViewerText;
    public Text commentIndex;
    public Image progressBar;

    void Start()
    {
        localMarkerDat = Application.persistentDataPath + Path.DirectorySeparatorChar + "Marker.dat";
        localMarkerXml = Application.persistentDataPath + Path.DirectorySeparatorChar + "Marker.xml";

        downloadFileMarkerDat = new GameObject("downloadFileMarkerDat").AddComponent<DownloadFile>();
        downloadFileMarkerXml = new GameObject("downloadFileMarkerXml").AddComponent<DownloadFile>();

        StartCoroutine(downloadFileMarkerDat.Download(remoteMarkerDat, localMarkerDat));
        StartCoroutine(downloadFileMarkerXml.Download(remoteMarkerXml, localMarkerXml));
    }

    void Update()
    {
        if (!areCustomTrackableEventHandlersInitialized &&
            downloadFileMarkerDat.success &&
            downloadFileMarkerXml.success)
        {
            areCustomTrackableEventHandlersInitialized = true;
            CreateCustomTrackableEventHandler(localMarkerXml);
        }
    }

    public static void CreateCustomTrackableEventHandler(string localMarkerXml)
    {
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();
        DataSet dataSet = objectTracker.CreateDataSet();

        if (dataSet.Load(localMarkerXml, VuforiaUnity.StorageType.STORAGE_ABSOLUTE))
        {
            objectTracker.Stop();

            if (!objectTracker.ActivateDataSet(dataSet))
            {
                Debug.Log("Failed to Activate DataSet: " + localMarkerXml);
                return;
            }

            if (!objectTracker.Start())
            {
                Debug.Log("Tracker Failed to restart");
                return;
            }

            IEnumerable<TrackableBehaviour> trackableBehaviours = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();
            foreach (TrackableBehaviour trackableBehaviour in trackableBehaviours)
            {
                trackableBehaviour.gameObject.name = trackableBehaviour.TrackableName;
                CustomTrackableEventHandler customTrackableEventHandler = trackableBehaviour.gameObject.AddComponent<CustomTrackableEventHandler>();

                customTrackableEventHandler.trackableBehaviour = trackableBehaviour;

                customTrackableEventHandler.userDataContainer = new GameObject();
                customTrackableEventHandler.userDataContainer.name = "User Data Container";
                                                        customTrackableEventHandler.userDataContainer.transform.SetParent(trackableBehaviour.gameObject.transform);
            }

            return;
        }
        else
        {
            Debug.Log("Failed to load dataset from '" + localMarkerXml + "'");
            return;
        }
    }


    public void OpenGetInvolved()
    {
        Application.OpenURL("https://rotondes.next-site.de/get-involved/");
    }

    public void OpenAbout()
    {
        Application.OpenURL("https://rotondes.next-site.de/about/");
    }

    public void OpenImpressum()
    {
        Application.OpenURL("https://rotondes.next-site.de/impressum/");
    }

    public void OpenPrivacy()
    {
        Application.OpenURL("https://rotondes.next-site.de/privacy/");
    }
}
