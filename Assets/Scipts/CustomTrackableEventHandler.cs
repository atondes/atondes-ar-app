﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using PostWithComments;
using UnityEngine;
using UnityEngine.Networking;
using Vuforia;

public class CustomTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{
    public TrackableBehaviour trackableBehaviour;
    public GameObject userDataContainer;
    public List<CommentHandler> commentHandlers;

    private static string remoteCommtentsJson = "https://rotondes.next-site.de/wp-json/atondes-wordpress-plugin/v1/getUserMediaForPost?ar_key=";
    public string commentsJson;
    private bool isInitialized = false;

    void Start()
    {
        trackableBehaviour.RegisterTrackableEventHandler(this);
        this.commentHandlers = new List<CommentHandler>();
    }

    static TrackableBehaviour previouslyTrackedBehaviour;

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            Debug.Log(trackableBehaviour.TrackableName + " is now tracked");

            userDataContainer.SetActive(true);
            Main.instance.currentTrackableWidth = this.GetComponent<ImageTargetBehaviour>().ImageTarget.GetSize().x;
            Main.instance.currentTrackableHeight = this.GetComponent<ImageTargetBehaviour>().ImageTarget.GetSize().y;
            Main.instance.currentCustomTrackableEventHandler = this;

            if (previouslyTrackedBehaviour != trackableBehaviour)
            {
                Debug.Log("Trackable changed");
                previouslyTrackedBehaviour = trackableBehaviour;

                if (!isInitialized)
                {
                    isInitialized = true;
                    StartCoroutine(DownloadComments(trackableBehaviour.TrackableName, userDataContainer.transform));
                }
            }
            else
            {
                Debug.Log("Trackable still the same");
            }
        }
        else
        {
            Debug.Log(trackableBehaviour.TrackableName + " is now lost");
            userDataContainer.SetActive(false);
        }
    }

    public IEnumerator DownloadComments(string meta_value, Transform parent)
    {
        UnityWebRequest unityWebRequest = new UnityWebRequest(remoteCommtentsJson + meta_value);
        unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
        yield return unityWebRequest.SendWebRequest();

        if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
        {
            Debug.Log("Unable to consume comments rest api '" + remoteCommtentsJson + "': unityWebRequest.error: " + unityWebRequest.error);
            yield break;
        }

        Debug.Log("Successfully consumed comments rest api '" + remoteCommtentsJson + meta_value + "'");

        string dataAsString = Encoding.UTF8.GetString(unityWebRequest.downloadHandler.data, 0, unityWebRequest.downloadHandler.data.Length);

        RootObject postWithComments = JsonUtility.FromJson<PostWithComments.RootObject>(dataAsString);

        Post post = postWithComments.postList.FirstOrDefault(x => x.meta_value == meta_value);

        if (post == null)
        {
            Debug.Log("Error, posts with meta_value " + meta_value + " could not be found");
            yield break;
        }

        Transform[] allChildren = parent.GetComponentsInChildren<Transform>(true);
        for (int i = 0; i < allChildren.Count(); i++)
        {
            if (allChildren[i].gameObject.transform != parent.transform)
            {
                Destroy(allChildren[i].gameObject);
            }
        }

        foreach (Comment comment in post.commentList)
        {
            GameObject newComment = new GameObject("comment-" + Path.GetFileName(comment.original_image));
            newComment.transform.parent = parent;
            CommentHandler newCommentHandler = newComment.AddComponent<CommentHandler>();
            newCommentHandler.comment = comment;
            StartCoroutine(newCommentHandler.GetAndSpawnComment());
            commentHandlers.Add(newCommentHandler);
        }
    }
}
