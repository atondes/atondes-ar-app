﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using PostWithComments;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CommentHandler : MonoBehaviour
{ 
    public Comment comment = null;
    public BoxCollider boxCollider = null;

    public const float previewImageScaleMax = 0.07f;
    public const float previewImageScaleMin = 0.04f;

    public const float maxRotation = 10f;

    public const int minIntFromHash = 00000000;
    public const int maxIntFromHash = 999999999;

    string remoteSmallImagePath;
    string localSmallImagePath;

    public IEnumerator GetAndSpawnComment()
    {
        localSmallImagePath = Application.persistentDataPath + Path.DirectorySeparatorChar;

        if (comment.preview_image != null && comment.preview_image != "")
        {
            localSmallImagePath += Path.GetFileName(comment.preview_image);
            remoteSmallImagePath = comment.preview_image;
        }
        else
        {
            // If uploaded images are too small, we need to use the image directly
            localSmallImagePath += Path.GetFileName(comment.original_image);
            remoteSmallImagePath = comment.original_image;
        }

        // If we have the file in local storage, we will reuse it.
        if (!File.Exists(localSmallImagePath))
        {
            UnityWebRequest unityWebRequest = new UnityWebRequest(remoteSmallImagePath);
            unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
 
            yield return unityWebRequest.SendWebRequest();

            if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
            {
                Debug.Log("Unable to download '" + remoteSmallImagePath + "': unityWebRequest.error: " + unityWebRequest.error);
                yield break;
            }
            else
            {
                Debug.Log("Successfully finished downloading '" + remoteSmallImagePath + "' to '" + localSmallImagePath + "'");
                File.WriteAllBytes(localSmallImagePath, unityWebRequest.downloadHandler.data);
            }
        }
        else
        {
            Debug.Log("Found file on filesystem '" + localSmallImagePath + "'");
        }

        float maxX = Main.instance.currentTrackableWidth / 2 - previewImageScaleMax * 2;
        float maxY = Main.instance.currentTrackableHeight / 2 - previewImageScaleMax * 2;

        // Load and set the preview image from the filesystem
        Texture2D arImage = new Texture2D(2, 2);
        ImageConversion.LoadImage(arImage, File.ReadAllBytes(localSmallImagePath));
        arImage.Apply();

        // Attach a SpriteRenderer to this scripts parent and load the preview image
        SpriteRenderer spriteRenderer = this.gameObject.AddComponent<SpriteRenderer>();
        spriteRenderer.sprite = Sprite.Create(arImage, new Rect(0, 0, arImage.width, arImage.height), new Vector2(.5f, .5f));

        // By initializing the random with the filename's hash, the picture will always appear in the same location
        float randomX = GetNumbersFromString(sha256_hash(remoteSmallImagePath + "x"));
        float randomY = GetNumbersFromString(sha256_hash(remoteSmallImagePath + "y"));
        float randomRotation = GetNumbersFromString(sha256_hash(remoteSmallImagePath + "r"));
        float randomSize = GetNumbersFromString(sha256_hash(remoteSmallImagePath + "s"));
        float x = MapRange(randomX, minIntFromHash, maxIntFromHash, -maxX, maxX);
        float y = MapRange(randomY, minIntFromHash, maxIntFromHash, -maxY, maxY);
        float r = MapRange(randomRotation, minIntFromHash, maxIntFromHash, -maxRotation, maxRotation);
        float s = MapRange(randomSize, minIntFromHash, maxIntFromHash, previewImageScaleMin, previewImageScaleMax);

        this.transform.localPosition = new Vector3(x, 0, y);
        this.transform.localRotation = Quaternion.Euler(90, r, 0);
        this.transform.localScale = new Vector3(s, s, s);

        // Add boxx collider to make the image clickable and to detect overlaps
        boxCollider = this.gameObject.AddComponent<BoxCollider>();

        int commentHandlerIndexToCheck = 0;
        int retries = 0;

        while (commentHandlerIndexToCheck <= Main.instance.currentCustomTrackableEventHandler.commentHandlers.Count - 1 && retries < 10)
        {
            CommentHandler commentHandlerToCheckAgainst = Main.instance.currentCustomTrackableEventHandler.commentHandlers[commentHandlerIndexToCheck];

            // Avoid check collision with itself because that results in always true
            if (commentHandlerToCheckAgainst == this || !commentHandlerToCheckAgainst.gameObject.activeSelf)
            {
                commentHandlerIndexToCheck++;
                continue;
            }

            if (commentHandlerToCheckAgainst.boxCollider && boxCollider.bounds.Intersects(commentHandlerToCheckAgainst.boxCollider.bounds))
            {
                retries++;

                float newRandomX = GetNumbersFromString(sha256_hash(remoteSmallImagePath + "x" + retries));
                float newRandomY = GetNumbersFromString(sha256_hash(remoteSmallImagePath + "y" + retries));
                float newX = MapRange(newRandomX, minIntFromHash, maxIntFromHash, -maxX, maxX);
                float newY = MapRange(newRandomY, minIntFromHash, maxIntFromHash, -maxY, maxY);

                // Change position if box collider intersecting with other collider
                this.transform.localPosition = new Vector3(newX, 0, newY);

                // We need to syn the Physics because otherwise the colliders would only update next frame
                Physics.SyncTransforms();

                // Start all over again
                commentHandlerIndexToCheck = 0;
            }
            else
            {
                commentHandlerIndexToCheck += 1;
            }
        }

        if (retries > 0)
        {
            Debug.Log(comment.original_image + " needed to be relocated " + retries + " times");
        }

        yield return null;
    }

    public IEnumerator GetAndShowLargeImage()
    {
        string remoteLargeImagePath;
        string localLargeImagePath = Application.persistentDataPath + Path.DirectorySeparatorChar;

        Main.instance.pictureViewerImage.GetComponent<Image>().color = Color.black;

        if (comment.large_image != null && comment.large_image != "")
        {
            localLargeImagePath += Path.GetFileName(comment.large_image);
            remoteLargeImagePath = comment.large_image;
        }
        else
        {
            // If uploaded images are too small, we need to use the image directly
            localLargeImagePath += Path.GetFileName(comment.original_image);
            remoteLargeImagePath = comment.original_image;
        }

        // If we have the file in local storage, we will reuse it.
        if (!File.Exists(localLargeImagePath))
        {
            UnityWebRequest unityWebRequest = new UnityWebRequest(remoteLargeImagePath);
            unityWebRequest.downloadHandler = new DownloadHandlerBuffer();

            // yield return unityWebRequest.SendWebRequest();

            Main.instance.progressBar.gameObject.SetActive(true);

            UnityWebRequestAsyncOperation unityWebRequestAsyncOperation = unityWebRequest.SendWebRequest(); 

            while (!unityWebRequestAsyncOperation.isDone)
            {
                Main.instance.progressBar.fillAmount = unityWebRequest.downloadProgress;
                yield return null;
            }

            Main.instance.progressBar.gameObject.SetActive(false);

            if (unityWebRequest.isNetworkError || unityWebRequest.isHttpError)
            {
                Debug.Log("Unable to download '" + remoteLargeImagePath + "': unityWebRequest.error: " + unityWebRequest.error);
                yield break;
            }
            else
            {
                Debug.Log("Successfully finished downloading '" + remoteLargeImagePath + "' to '" + localLargeImagePath + "'");
                File.WriteAllBytes(localLargeImagePath, unityWebRequest.downloadHandler.data);
            }
        }
        
        Texture2D largeImage = new Texture2D(2, 2);

        ImageConversion.LoadImage(largeImage, File.ReadAllBytes(localLargeImagePath));
        largeImage.Apply();

        Image image = Main.instance.pictureViewerImage.GetComponent<Image>();

        image.sprite = Sprite.Create(largeImage, new Rect(0, 0, largeImage.width, largeImage.height), new Vector2(.5f, .5f));
        image.preserveAspect = true;

        Main.instance.pictureViewerText.text = comment.text;
        Main.instance.commentIndex.text = (Main.instance.currentCustomTrackableEventHandlerInPictureViewer.commentHandlers.IndexOf(this) + 1) + "/" + Main.instance.currentCustomTrackableEventHandlerInPictureViewer.commentHandlers.Count;

        Main.instance.pictureViewerImage.GetComponent<Image>().color = Color.white;

        yield return null;
    }

    public static int GetNumbersFromString (string inputString)
    {
        char[] inputStringAsCharArray = inputString.ToArray();
        string numbersFromCharArray = "";

        for (int i = 0; i < inputStringAsCharArray.Length; i++)
        {
            if (Char.IsDigit(inputStringAsCharArray[i]))
            {
                numbersFromCharArray += inputStringAsCharArray[i];
            }

            // We need to stay below int max
            if (numbersFromCharArray.Length >= int.MaxValue.ToString().Length - 1)
            {
                break;
            }
        }

        return int.Parse(numbersFromCharArray);
    }

    public static float MapRange(float value, float inputRangeFrom, float inputRangeTo, float outputRangeFrom, float outputRangeTo)
    {
        return (value - inputRangeFrom) * (outputRangeTo - outputRangeFrom) / (inputRangeTo - inputRangeFrom) + outputRangeFrom;
    }

    // https://stackoverflow.com/questions/16999361/obtain-sha-256-string-of-a-string/17001289
    public static string sha256_hash(string value)
    {
        using (SHA256 hash = SHA256Managed.Create())
        {
            return string.Concat(hash
              .ComputeHash(Encoding.UTF8.GetBytes(value))
              .Select(item => item.ToString("x2")));
        }
    }
}
